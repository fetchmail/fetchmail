podman run --rm -it debian:testing sh -c '
	apt update -y && 
	apt upgrade -y &&
	apt install --no-install-recommends build-essential libssl-dev git ca-certificates bison flex htmldoc pandoc meson pkgconf python3-full python3-pip -y &&
	python3 -mvenv pyvenv-fetchmail &&
    	pyvenv-fetchmail/bin/python -mpip install rst2html5 asciidoc &&
	export PATH=$PATH:$(pwd)/pyvenv-fetchmail/bin && 
	git clone -b legacy_6x https://gitlab.com/fetchmail/fetchmail.git &&
	cd fetchmail &&
	meson setup _build &&
	ninja -C _build dist|| sh -l'
