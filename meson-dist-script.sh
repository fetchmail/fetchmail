#!/bin/sh

set -xeu

cd "${MESON_DIST_ROOT}"
filelist="\
	rcfile_l.h \
	rcfile_l.c \
	rcfile_y.h \
	rcfile_y.c \
	Mailbox-Names-UTF7.html \
	fetchmail-man.html \
	fetchmail-FAQ.pdf \
"
ninja -C "${MESON_BUILD_ROOT}" $filelist
for i in $filelist ; do
	cp -p "${MESON_BUILD_ROOT}"/$i .
done
(TZ=UTC LC_ALL=C date >fetchmail.tarball)
