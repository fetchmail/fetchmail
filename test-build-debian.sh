#! /bin/sh
set -xue
podman pull debian:testing
podman run --rm -it debian:testing sh -c '
	apt update -y && 
	apt upgrade -y &&
	apt install --no-install-recommends build-essential autoconf automake autopoint gettext libssl-dev m4 bison flex git ca-certificates pkg-config -y &&
	git clone https://gitlab.com/fetchmail/fetchmail.git &&
	cd fetchmail &&
	autoreconf -svif &&
	./configure -C &&
	make -sj20 check &&
	./fetchmail -V'
